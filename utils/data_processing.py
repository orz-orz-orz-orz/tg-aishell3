import pathlib
import torchaudio
import hanlp

src_dir = pathlib.Path("downloads/data_aishell3/train")
tgt_dir = pathlib.Path("data/train")


effects = [
    ['remix', '1'],     # select left channel
    ['rate', '16000'],  # resample to 16000 Hz
]


# read the content.txt
content = (src_dir/"content.txt").open('r').readlines()

# load HANLP
HanLP = hanlp.load(hanlp.pretrained.mtl.CLOSE_TOK_POS_NER_SRL_DEP_SDP_CON_ELECTRA_BASE_ZH)
tasks = list(HanLP.tasks.keys())
print(tasks)  # Pick what you need from what we have
for task in tasks:
    if task not in ('tok/fine'):
        del HanLP[task]

# create transcripts
for line in content:
    tokens = line.split()
    filename = tokens[0][0:11]
    dirname = filename[0:7]
    raw_text = "".join(tokens[1::2])
    pinyin = " ".join(tokens[2::2])
    text = " ".join(HanLP(raw_text, tasks='tok')['tok/fine'])

    print(f"{filename} {text}")

    src_wav = src_dir/ 'wav' / dirname / f"{filename}.wav"
    tgt_wav = tgt_dir/ 'wav-16k' / dirname / f"{filename}.wav"
    tgt_lab = tgt_dir/ 'wav-16k' / dirname / f"{filename}.txt"
    tgt_txt = tgt_dir / 'txt' / dirname / f"{filename}.txt"
    
    (tgt_dir/'wav-16k'/dirname).mkdir(parents=True, exist_ok=True)
    (tgt_dir/'txt'/dirname).mkdir(parents=True, exist_ok=True)
    
    wav, sr = torchaudio.sox_effects.apply_effects_file(src_wav, effects)
    torchaudio.save(tgt_wav, wav, sr)
    tgt_txt.open("w").write(text)
    tgt_lab.open("w").write(pinyin)

