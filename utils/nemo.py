import pathlib
# NeMo's "core" package
import nemo
# NeMo's ASR collection - this collections contains complete ASR models and
# building blocks (modules) for ASR
import nemo.collections.asr as nemo_asr

src_dir = pathlib.Path("data/train")

citrinet = nemo_asr.models.EncDecCTCModel.from_pretrained(model_name="stt_zh_citrinet_512")

files = [ str(x) for x in (src_dir/"wav-16k").glob("**/*.wav") ]
for fp, transcription in zip(files, citrinet.transcribe(paths2audio_files=files, return_hypotheses=False )):
    fp = pathlib.Path(fp)
    filename = fp.stem
    dirname = fp.parent.name
    tgt_txt = src_dir / 'nemo' / dirname / f"{filename}.txt"
    (src_dir / 'nemo' / dirname).mkdir(parents=True, exist_ok=True)
    tgt_txt.open("w").write(transcription)
    print(f"{filename} => {transcription}")