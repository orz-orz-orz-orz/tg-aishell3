# TG-AISHELL3

## 說明
這個專案是用來製作 AISHELL3 的 TextGrid。
會使用 NeMo 去掉一些可能唸不好或標錯的的句子，再用 HanLP 分詞 + POS Tagging。 
POS 格式是用 Chinese Treebank。

再來是使用 MFA 對句子以音節/聲母韻母為單位進行切割，再與斷詞結果結合，輸出 TextGrid。

產生出的 TextGrid 會有 四個階層 sentences/words/syllables/phones

注意，產出的 TextGrid 不包含有兒化音的句子。


## TextGrid 會放在 release page 中
